int NR_OF_PEOPLE = 200;
int NR_OF_FRIENDS = 5;

Group group;
PFont font;


void setup() {
  size(640,480,P3D);
  ellipseMode(CENTER_RADIUS);

  font = loadFont("Aldine401BT-BoldA-15.vlw");
  textFont(font,15);

  group = new Group();

  NameList nl = new NameList();
  //nl.reduceTo(500);

  // CREATE PEOPLE:
  float minDistSq = 6*6;
  while (group.size()<NR_OF_PEOPLE) {
    Person p = new Person(group,nl.getAvailableName());
    boolean overlap=true;
    while (overlap) {
      p.X=random(width);
      p.Y=random(height);
      overlap=false;
      for (int i=0;i<group.size();i++) {
        Person q = group.getPersonAt(i);
        float dx = p.X-q.X, dy = p.Y-q.Y;
        if (dx*dx+dy*dy<minDistSq) overlap=true;
      }
    }
    group.add(p);
  }

  // MAKE FRIENDS:

  for (int i=0;i<NR_OF_PEOPLE;i++) {
    Person A = group.getPersonAt(i);
    Person B = group.getPersonAt(i);
    for (int f=0;f<NR_OF_FRIENDS;f++) {
      int j=i;
      while (j==i) j=(int)random(group.size());
      B = group.getPersonAt(j);
      if (A.FRIENDS.size()<NR_OF_FRIENDS&&B.FRIENDS.size()<NR_OF_FRIENDS)  {
        boolean success = A.makeFriendship(B.NAME);
        if (!success) f--;
      }
    }
  }

  Person p = new Person(group,"Marcus");
  p.X = width/2.0;
  p.Y = height/2.0;
  group.add(p);
  for (int i=0;i<NR_OF_FRIENDS;i++) {
    int j=(int)random(group.size());
    Person B = group.getPersonAt(j);
    while (p.equals(B)&&B.FRIENDS.size()>=NR_OF_FRIENDS) {
      j=(int)random(group.size());
      B = group.getPersonAt(j);
    }
    p.makeFriendship(B.NAME);
  }
  
}

void mousePressed() {
  setup();
}

void draw() {
  colorMode(RGB,255);
  background(0);
    
  //group.setSourcePerson(group.getPersonClosestToMouse());
  group.setSourcePerson(group.getPersonAt((int)((group.size()-2)*((float)mouseX/width))));
  group.setTargetPerson(group.getPersonNamed("Marcus"));
  group.draw();
  
  (group.source).drawFriends();
  (group.target).drawFriends();
    
  stroke(128,128,128);
  group.showSourceToTarget(6,false);  
}
