import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class AFriendOfAFriendV2_2 extends PApplet {int NR_OF_PEOPLE = 200;
int NR_OF_FRIENDS = 5;

Group group;
PFont font;


public void setup() {
  size(640,480,P3D);
  ellipseMode(CENTER_RADIUS);

  font = loadFont("Aldine401BT-BoldA-15.vlw");
  textFont(font,15);

  group = new Group();

  NameList nl = new NameList();
  //nl.reduceTo(500);

  // CREATE PEOPLE:
  float minDistSq = 6*6;
  while (group.size()<NR_OF_PEOPLE) {
    Person p = new Person(group,nl.getAvailableName());
    boolean overlap=true;
    while (overlap) {
      p.X=random(width);
      p.Y=random(height);
      overlap=false;
      for (int i=0;i<group.size();i++) {
        Person q = group.getPersonAt(i);
        float dx = p.X-q.X, dy = p.Y-q.Y;
        if (dx*dx+dy*dy<minDistSq) overlap=true;
      }
    }
    group.add(p);
  }

  // MAKE FRIENDS:

  for (int i=0;i<NR_OF_PEOPLE;i++) {
    Person A = group.getPersonAt(i);
    Person B = group.getPersonAt(i);
    for (int f=0;f<NR_OF_FRIENDS;f++) {
      int j=i;
      while (j==i) j=(int)random(group.size());
      B = group.getPersonAt(j);
      if (A.FRIENDS.size()<NR_OF_FRIENDS&&B.FRIENDS.size()<NR_OF_FRIENDS)  {
        boolean success = A.makeFriendship(B.NAME);
        if (!success) f--;
      }
    }
  }

  Person p = new Person(group,"Marcus");
  p.X = width/2.0f;
  p.Y = height/2.0f;
  group.add(p);
  for (int i=0;i<NR_OF_FRIENDS;i++) {
    int j=(int)random(group.size());
    Person B = group.getPersonAt(j);
    while (p.equals(B)&&B.FRIENDS.size()>=NR_OF_FRIENDS) {
      j=(int)random(group.size());
      B = group.getPersonAt(j);
    }
    p.makeFriendship(B.NAME);
  }
  
}

public void mousePressed() {
  setup();
}

public void draw() {
  colorMode(RGB,255);
  background(0);
    
  //group.setSourcePerson(group.getPersonClosestToMouse());
  group.setSourcePerson(group.getPersonAt((int)((group.size()-2)*((float)mouseX/width))));
  group.setTargetPerson(group.getPersonNamed("Marcus"));
  group.draw();
  
  (group.source).drawFriends();
  (group.target).drawFriends();
    
  stroke(128,128,128);
  group.showSourceToTarget(6,false);  
}

class Group extends Vector {
  
  Person source, target;
  
  Group() {
    super();
  }
  
  public Person getPersonAt(int i) {
    return (Person)elementAt(i);
  }
  
  public Person getPersonNamed(String n) {
    for (int i=0;i<size();i++) {
      if (((Person)elementAt(i)).NAME.equals(n)) return ((Person)elementAt(i));
    }
    return null;
  }
  
  public Person getPersonClosestToMouse() {
    float minDistSq=9999*9999;
    int closestI=-1;
    for (int i=0;i<size();i++) {
      Person p = getPersonAt(i);
      float dx = mouseX-p.X;
      float dy = mouseY-p.Y;
      float distSq = dx*dx+dy*dy;
      if (distSq<minDistSq) {
        minDistSq=distSq;
        closestI=i;
      }
    }
    Person p = getPersonAt(closestI);
    return p;
  }
  
  public void setTargetPerson(Person p) {
    target=p;
  }
  
  public void setSourcePerson(Person p) {
    source=p;
  }
    
  public void showSourceToTarget(int maxlinks, boolean exact_number) {
    Person[] visited = new Person[0];
    if (!source.equals(target))
      source.areYouOrDoYouKnow(target,maxlinks-1,visited, exact_number);
  }
  
  public void draw() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).drawDot(false);
    source.drawDot(true);
    target.drawDot(true);
    source.drawName();
    target.drawName();
  }
  
}

class NameList {
  
  String[] NAMES;
  boolean[] used;
  
  NameList() {
    print("Loading names: ");
    NAMES = loadStrings("names.TXT");
    int a = NAMES.length;
    println(a+" names.");
    used = new boolean[a];
    for (int i=0; i<a; i++) used[i]=false;
  }
  
  public void reduceTo(int a) {
    String[] newNames = new String[a];
    for (int i=0;i<newNames.length;i++) newNames[i]=NAMES[i];
    NAMES = new String[a];
    for (int i=0;i<newNames.length;i++) NAMES[i]=newNames[i];
    println("Namelist reduced to "+NAMES.length+" names.");
  }
  
  public String getAnyName() {
    int n = (int)random(NAMES.length);
    used[n] = true;
    return NAMES[n];
  }
  
  public String getAvailableName() {
    boolean found=false;
    int n=0;
    while (!found) {
      n=(int)random(NAMES.length);
      if (!used[n]) {
        used[n]=true;
        found=true;
      }
    }
    return NAMES[n];
  }
  
}

class Person {

  String NAME;
  Vector FRIENDS = new Vector();
  float X, Y;

  Group group;

  Person(Group _group, String n) {
    group = _group;
    NAME = n;
    X = random(width);
    Y = random(height);
  }
  
  public boolean makeFriendship(String n) {
    Person p = group.getPersonNamed(n);
    if (p==this) return false;
    if (p==null) {
      //println("creating new person as friend");
      p = new Person(group,n);
      group.add(p);
      float rd = random(TWO_PI);
      float r = random(20,50);
      p.X = X + r*cos(rd);
      p.Y = Y + r*sin(rd);
      if (p.X<0) p.X=0;
      if (p.X>width) p.X=width;
      if (p.Y<0) p.Y=0;
      if (p.Y>height) p.Y=height;
    } 
    else {
      //println("linking to existing person");
    }
    if (FRIENDS.contains(p)) {
      //println("friendship already exists!");
      return false;
    }
    FRIENDS.add(p);
    p.FRIENDS.add(this);
    return true;
  }


  public boolean areYouOrDoYouKnow(Person target, int maxlinks, Person[] _visited, boolean exact_number) {

    for (int v=0;v<_visited.length;v++) if (_visited[v].equals(this)) return false;
    
    Person[] visited = new Person[_visited.length];
    for (int i=0;i<_visited.length;i++) visited[i]=_visited[i];
    append(visited,this);

    boolean out = false;
    
    if (exact_number) {
      if (maxlinks==0) if (target.equals(this)) out = true;
    } 
    else {
      if (target.equals(this)) {
        out = true;
      }
    }


    if (maxlinks>0) {
      for (int i=0;i<FRIENDS.size();i++) {
        for (int v=0;v<visited.length;v++) if (((Person)FRIENDS.elementAt(i)).equals(visited[v])) return false;
        Person friend = (Person)FRIENDS.elementAt(i);
        if (friend.areYouOrDoYouKnow(target,maxlinks-1,visited,exact_number)) {
          line(X,Y,friend.X,friend.Y);
          out = true;
        }
      }
    }

    return out;
  }

  public void drawDot(boolean active) {
    if (active) stroke(0); 
    else noStroke();
    fill(255,0,0);
    ellipse(X,Y,3,3);
  }

  public void drawName() {
    fill(255,255,0);
    text(NAME,(int)X+3,(int)Y-3);
  }

  public void drawFriends() {
    stroke(0,128,0,64);
    for (int i=0;i<FRIENDS.size();i++) {
      Person f = (Person)FRIENDS.elementAt(i);
      line(X,Y,f.X,f.Y);
    }
  }

}

/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  double x=0,y=0,z=0;
  
  Vec() {
  }
  
  Vec(double _x, double _y) {
    x=_x;
    y=_y;
  }
  
  Vec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  Vec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  public void setVec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  public void setVec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  public double len() {
    return Math.sqrt(x*x+y*y+z*z);
  }
  
  public double lenSQ() {
    return (x*x+y*y+z*z);
  }
  
  public void gravitateTo(Vec _to, double _mgd, double _fFac) {
    double minGravDist2=_mgd*_mgd;
    double dx=_to.x-x;
    double dy=_to.y-y;
    double dz=_to.z-z;
    double d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {d2=minGravDist2;}
    //double d=Math.sqrt(d2);
    double F=_fFac/d2;
    add(new Vec(dx*F,dy*F,dz*F));
  }

  public void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }
  
  public void add(double _x, double _y, double _z) {
    x+=_x;
    y+=_y;
    z+=_z;
  }

  public void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  public void mul(double p) {
    x*=p;
    y*=p;
    z*=p;
  }
  
  public void div(double p) {
    x/=p;
    y/=p;
    z/=p;
  }

  public void negX() {
    x=-x;
  }

  public void negY() {
    y=-y;
  }

  public void negZ() {
    z=-z;
  }

  public void neg() {
    negX();
    negY();
    negZ();
  }
  
  public void normalize() {
    double l=len();
    if (l!=0) { // if not nullvector
      div(l);
    }
  }
  
  public Vec getNormalized() {
    double l=len();
    if (l!=0) {
      Vec out=new Vec(this);
      out.div(l);
      return out;
    } else {
      return new Vec();
    }
  }
  
  public void setLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/len();
    x*=fac;
    y*=fac;
    z*=fac;
  }
  
  public void toAvLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/((ml+len())/2.0f);
    x*=fac;
    y*=fac;
    z*=fac;
  }

  public void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double yn=y*COS-z*SIN;
    double zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  public void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-z*SIN; 
    double zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  public void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-y*SIN; 
    double yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  public void rot(double xrd, double yrd, double zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }
  
  public boolean isNullVec() {
    if (x==0&&y==0&&z==0) {return true;} else {return false;}
  }
}




public Vec vecAdd(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.add(b);
  return out;
}

public Vec vecSub(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.sub(b);
  return out;
}

public Vec vecMul(Vec a, double b) {
  Vec out=new Vec(a);
  out.mul(b);
  return out;
}

public Vec vecDiv(Vec a, double b) {
  Vec out=new Vec(a);
  out.div(b);
  return out;
}

public double vecLen(Vec a) {
  return a.len();
}

public Vec vecRotY(Vec a, double rd) {
  Vec out=new Vec(a);
  out.rotY(rd);
  return out;
}

public Vec rndDirVec(double r) {
  Vec out=new Vec(0,0,r);
  out.rotX(random(-PI/2.0f,PI));
  out.rotZ(random(0,2*PI));
  return out;
}

public Vec rndPlusMinVec(double s) {
  Vec out=new Vec(-s+2*s*Math.random(),-s+2*s*Math.random(),-s+2*s*Math.random());
  return out;
}

public void vecSwap(Vec v1, Vec v2) {
  Vec t=new Vec(v1);
  v1.setVec(v2);
  v2.setVec(t);
}

/*****************************
 * adapted 3d functions to Vec
 *****************************/

public void vecTranslate(Vec v) {
  translate((float)v.x,(float)v.y,(float)v.z);
}

public void vecVertex(Vec v) {
  vertex((float)v.x,(float)v.y,(float)v.z);
}

public void vecLine(Vec a, Vec b) {
  line((float)a.x,(float)a.y,(float)a.z,(float)b.x,(float)b.y,(float)b.z);
}

public void vecRect(Vec a, Vec b) {
  rect((float)a.x,(float)a.y,(float)b.x,(float)b.y);
}

public void vecCamera(Vec eye, Vec cen, Vec upaxis) {
  camera((float)eye.x,(float)eye.y,(float)eye.z,(float)cen.x,(float)cen.y,(float)cen.z,(float)upaxis.x,(float)upaxis.y,(float)upaxis.z);
}
static public void main(String args[]) {   PApplet.main(new String[] { "AFriendOfAFriendV2_2" });}}