import java.util.Vector;
class Group extends Vector {
  
  Person source, target;
  
  Group() {
    super();
  }
  
  Person getPersonAt(int i) {
    return (Person)elementAt(i);
  }
  
  Person getPersonNamed(String n) {
    for (int i=0;i<size();i++) {
      if (((Person)elementAt(i)).NAME.equals(n)) return ((Person)elementAt(i));
    }
    return null;
  }
  
  Person getPersonClosestToMouse() {
    float minDistSq=9999*9999;
    int closestI=-1;
    for (int i=0;i<size();i++) {
      Person p = getPersonAt(i);
      float dx = mouseX-p.X;
      float dy = mouseY-p.Y;
      float distSq = dx*dx+dy*dy;
      if (distSq<minDistSq) {
        minDistSq=distSq;
        closestI=i;
      }
    }
    Person p = getPersonAt(closestI);
    return p;
  }
  
  void setTargetPerson(Person p) {
    target=p;
  }
  
  void setSourcePerson(Person p) {
    source=p;
  }
    
  void showSourceToTarget(int maxlinks, boolean exact_number) {
    Person[] visited = new Person[0];
    if (!source.equals(target))
      source.areYouOrDoYouKnow(target,maxlinks-1,visited, exact_number);
  }
  
  void draw() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).drawDot(false);
    source.drawDot(true);
    target.drawDot(true);
    source.drawName();
    target.drawName();
  }
  
}
