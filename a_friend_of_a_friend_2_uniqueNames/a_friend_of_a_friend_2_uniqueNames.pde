Group group = new Group();
int AMOUNT_OF_PEOPLE = 500;
int AMOUNT_OF_FRIENDS = 3;
int MAXLINK = 6;
PFont font;

int fredId;

void setup() {
  
  size(400,300,JAVA2D);
  ellipseMode(CENTER);
  font = loadFont("ArialMT-12.vlw");
  textFont(font,12);
  
  //curve
  group.initNames();
  //group.create(AMOUNT_OF_PEOPLE);  
  //group.makeRandomFriendships(AMOUNT_OF_PEOPLE*AMOUNT_OF_FRIENDS/2);
  //group.makeFriends(AMOUNT_OF_FRIENDS);
  //group.save("group"+AMOUNT_OF_PEOPLE+".txt");
  
  //group.load("group500.txt");
  
  fredId = (int)random(group.size());
}


//void mousePressed() {
//  group.toggleShowStatus();
//}

void keyPressed() {
  if (key=='a') {
    group.setShowStatus(Person.LINKS_SHOW_ALL);
  } 
  else if (key=='s') {
    group.setShowStatus(Person.LINKS_SHOW_FIRST);
  }
  else if (key==' ') {
    fredId = (int)random(group.size());
  }
}

void draw() {
  colorMode(RGB,255);
  background(32,32,32);
  noStroke();
  group.draw();
  Person fred = (Person)group.elementAt(fredId);
  group.drawNameAndFriends(fredId);
  group.makeIndexOfMouseover();
  
  colorMode(HSB,255);
  if (group.INDEX_OF_MOUSEOVER!=-1&&group.INDEX_OF_MOUSEOVER!=fredId) {
    int i = group.INDEX_OF_MOUSEOVER;
    group.drawNameAndFriends(i);
    Person mousePerson = (Person)group.elementAt(i);
    mousePerson.startSearchFor(group,fred,MAXLINK-1);
  }
  
  
  
}


color morphColor(color c) {
  float h = hue(c);
  h+=10;
  if (h>255) h-=255;
  return color(h,saturation(c),brightness(c));
}
