import java.util.Vector;
class Group extends Vector {

  //float FRIEND_DISTANCE_DIVIDER = 1;
  String[] boyName, girlName;


  int INDEX_OF_MOUSEOVER=-1;

  Group() {
    super();
  }

  void initNames() {
    // load names
    // http://www.listofbabynames.org/k_girls.htm
    boyName = loadStrings("boys.TXT");
    girlName = loadStrings("girls.TXT");
    println(boyName.length+" boy names and "+girlName.length+" girl names");
  }

  void create(int amount) {
    // CREATE GROUP
    println("creating group");
    for (int i=0;i<amount;i++) {
      
      String name="";
      boolean nameExists=true;
      while (nameExists) {
        name = (random(1.0)<0.5)?boyName[(int)random(boyName.length)]:girlName[(int)random(girlName.length)];
        name+=char((int)random(65,65+25));
        name+=char((int)random(65,65+25));
        name+=char((int)random(65,65+25));
        if (size()==0) {
          nameExists=false;
        } else {
          nameExists=false;
          for (int j=0;j<size();j++) if (((Person)elementAt(j)).NAME.equals(name)) nameExists=true;
        }
      }
      

      boolean overlay=true;
      float x=0,y=0;
      while (overlay) {
        overlay=false;
        x=random(width);
        y=random(height);
        float minDSq=sq(2*Person.DRAW_SIZE);
        for (int t=0;t<size();t++) {
          if (sq(((Person)elementAt(t)).X-x)+sq(((Person)elementAt(t)).Y-y)<minDSq) {
            t=size();
            overlay=true;
          }
        }
      }

      add(new Person(name,x,y,i));
    }
  }

  /*
  void makeRandomFriendships(int amount) {
    for (int i=0;i<amount;i++) {
      // find two people:
      Person p1 = ((Person)elementAt((int)random(size())));
      Person p2 = new Person();
      boolean same=true;
      while (same) {
        p2 = ((Person)elementAt((int)random(size())));
        if (!p2.equals(p1)) same=false;
      }
      // make friends
      p1.FRIENDS = append(p1.FRIENDS,p2.NAME);
      p2.FRIENDS = append(p2.FRIENDS,p1.NAME);
      
    }
    
  }

  void makeFriends(int amount) {
    // MAKE FRIENDS:
    println("prepare making friends");


    float gridSize=30;
    Vector[][] spaceGrid=new Vector[(int)(width/gridSize)+1][(int)(height/gridSize)+1];
    for (int x=0;x<spaceGrid.length;x++) for (int y=0;y<spaceGrid[x].length;y++) spaceGrid[x][y]=new Vector();
    for (int i=0;i<size();i++) {
      Person p = (Person)elementAt(i);
      spaceGrid[(int)(p.X/gridSize)][(int)(p.Y/gridSize)].add(p);
    }

    println("making friends");
    float maxDist = sqrt(sq(width)+sq(height));
    for (int i=0;i<size();i++) {
      // FOR EACH PERSON IN GROUP:
      Person person = (Person)elementAt(i);

      while (person.FRIENDS.length<amount) {
        boolean foundFriend=false;
        int searchArea=1;
        while (!foundFriend) {
          Vector neighbours = new Vector();
          while (neighbours.size()==0) {
            neighbours=new Vector();
            for (int ix=(int)(person.X/gridSize)-searchArea;ix<=(int)(person.X/gridSize)+searchArea;ix++) {
              for (int iy=(int)(person.Y/gridSize)-searchArea;iy<=(int)(person.Y/gridSize)+searchArea;iy++) {
                if (ix>=0&&ix<spaceGrid.length&&iy>=0&&iy<spaceGrid[0].length) neighbours.addAll(spaceGrid[ix][iy]);
              }
            }
            searchArea++;
          }
          
          boolean newFriendFound=false;
          int tries=20;
          while (!newFriendFound&&tries>0) {
            Person newFriend = (Person)neighbours.elementAt((int)random(neighbours.size()));
            if ((!newFriend.equals(person))&&(!person.friendsContain(newFriend)&&(newFriend.FRIENDS.length<amount))) {
              person.friendAdd(newFriend);
              newFriend.friendAdd(person); 
              newFriendFound=true;
              foundFriend=true;
            } 
            tries--;
          }
          if (!newFriendFound) searchArea++;

        }
      }

    }

  }
  */

  void save(String filename) {
    println("preparing file");
    String[] outStrings = new String[size()];
    for (int i=0;i<size();i++) {
      Person p=(Person)elementAt(i);
      String s="";
      s+=p.UNIQUE_ID+"\t";
      s+=p.NAME+"\t";
      s+=p.X+"\t";
      s+=p.Y+"\t";
      for (int f=0;f<p.FRIENDS.length;f++) {
        Person friend=p.friendToPerson(this,f);
        s+=friend.UNIQUE_ID+"\t";
      }
      outStrings[i]=s;
    }
    println("saving "+filename);
    saveStrings(filename,outStrings);
  }
  
  void load(String filename) {
    String[] inStrings = loadStrings(filename);
    int amount = inStrings.length;
    for (int i=0;i<amount;i++) {
      String[] tab=split(inStrings[i],"\t");
      int id = int(tab[0]);
      String name = tab[1];
      float x = float(tab[2]);
      float y = float(tab[3]);
      add(new Person(name,x,y,id));
    }
    
    for (int i=0;i<amount;i++) {
      String[] tab=split(inStrings[i],"\t");
      int amountOfFriends = tab.length-4;
      for (int f=0;f<amountOfFriends;f++) {
        int friendId = int(tab[4+f]);
        ((Person)elementAt(i)).friendAdd((Person)elementAt(friendId));
      }
      
    }
  }


  void setShowStatus(int ls) {
    for (int i=0;i<size();i++)
      ((Person)elementAt(i)).LINKS_SHOW=ls;
  }
  void toggleShowStatus() {
    int ls = ((Person)elementAt(0)).LINKS_SHOW;
    ls++;
    if (ls>Person.LINKS_SHOW_FIRST) ls=0;
    setShowStatus(ls);
  }


  void draw() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).draw();
  }

  void drawNameAndFriends(int i) {
    Person p = ((Person)elementAt(i));
    p.drawNameAndFriends(this);
  }

  void makeIndexOfMouseover() {
    INDEX_OF_MOUSEOVER=-1;
    for (int i=0;i<size();i++) {
      Person p = ((Person)elementAt(i));
      if (p.mouseWithinRange()) INDEX_OF_MOUSEOVER = i;
    }
  }

}
