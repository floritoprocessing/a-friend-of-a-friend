class Person {

  String NAME;
  Vector FRIENDS = new Vector();
  float X, Y;

  Group group;
  Node NODE;

  Person(Group _group, String n) {
    group = _group;
    NAME = n;
    X = random(width);
    Y = random(height);
    NODE = new Node(new Vec(X,Y,0));
  }
  
  void makePositionFromNode() {
    X = (float)NODE.POSITION.x;
    Y = (float)NODE.POSITION.y;
  }

  boolean makeFriendship(String n) {
    Person p = group.getPersonNamed(n);
    if (p==this) return false;
    if (p==null) {
      //println("creating new person as friend");
      p = new Person(group,n);
      group.add(p);
      float rd = random(TWO_PI);
      float r = random(20,50);
      p.X = X + r*cos(rd);
      p.Y = Y + r*sin(rd);
      if (p.X<0) p.X=0;
      if (p.X>width) p.X=width;
      if (p.Y<0) p.Y=0;
      if (p.Y>height) p.Y=height;
    } 
    else {
      //println("linking to existing person");
    }
    if (FRIENDS.contains(p)) {
      //println("friendship already exists!");
      return false;
    }
    FRIENDS.add(p);
    p.FRIENDS.add(this);
    group.springs.add(new Spring(NODE,p.NODE));
    return true;
  }


  boolean areYouOrDoYouKnow(Person target, int maxlinks, Person[] _visited, boolean exact_number) {

    for (int v=0;v<_visited.length;v++) if (_visited[v].equals(this)) return false;
    
    Person[] visited = new Person[_visited.length];
    for (int i=0;i<_visited.length;i++) visited[i]=_visited[i];
    append(visited,this);

    boolean out = false;
    
    if (exact_number) {
      if (maxlinks==0) if (target.equals(this)) out = true;
    } 
    else {
      if (target.equals(this)) out = true;
    }


    if (maxlinks>0) {
      for (int i=0;i<FRIENDS.size();i++) {
        for (int v=0;v<visited.length;v++) if (((Person)FRIENDS.elementAt(i)).equals(visited[v])) return false;
        Person friend = (Person)FRIENDS.elementAt(i);
        if (friend.areYouOrDoYouKnow(target,maxlinks-1,visited,exact_number)) {
          line(X,Y,friend.X,friend.Y);
          out = true;
        }
      }
    }

    return out;
  }

  void drawDot(boolean active) {
    if (active) stroke(0); 
    else noStroke();
    fill(255,0,0);
    ellipse(X,Y,3,3);
  }

  void drawName() {
    fill(255,255,0);
    text(NAME,(int)X+3,(int)Y-3);
  }

  void drawFriends() {
    stroke(0,128,0,64);
    for (int i=0;i<FRIENDS.size();i++) {
      Person f = (Person)FRIENDS.elementAt(i);
      line(X,Y,f.X,f.Y);
    }
  }

}
