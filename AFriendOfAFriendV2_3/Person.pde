class Person {

  String NAME;
  Vector FRIENDS = new Vector();
  float X, Y;

  Group group;

  Person(Group _group, String n) {
    group = _group;
    NAME = n;
    X = random(width);
    Y = random(height);
  }
  
  boolean makeFriendship(String n) {
    Person p = group.getPersonNamed(n);
    if (p==this) return false;
    if (p==null) {
      //println("creating new person as friend");
      p = new Person(group,n);
      group.add(p);
      float rd = random(TWO_PI);
      float r = random(20,50);
      p.X = X + r*cos(rd);
      p.Y = Y + r*sin(rd);
      if (p.X<0) p.X=0;
      if (p.X>width) p.X=width;
      if (p.Y<0) p.Y=0;
      if (p.Y>height) p.Y=height;
    } 
    else {
      //println("linking to existing person");
    }
    if (FRIENDS.contains(p)) {
      //println("friendship already exists!");
      return false;
    }
    FRIENDS.add(p);
    p.FRIENDS.add(this);
    return true;
  }


  boolean areYouOrDoYouKnow(Person target, int maxlinks, Vector _path, boolean exact_number) {

    if (_path.contains(this)) return false;
    
    Vector path = new Vector();
    for (int i=0;i<_path.size();i++) path.add(_path.elementAt(i));
    path.add(this);

    boolean out = false;
    
    if (exact_number) {
      if (maxlinks==0) {
        if (target.equals(this)) out = true;
      }
    } 
    else {
      if (target.equals(this)) {
        out = true;
      }
    }
    
    // if final elemen of path: draw it!
    if (out==true) {
      
      // make unique color from path:
      int c = 0;
      for (int i=0;i<path.size();i++) c+=group.indexOf(path.elementAt(i));
      c = c%0xFF;
      colorMode(HSB,255);
      stroke(c,255,255,128);
      for (int i=0;i<path.size()-1;i++) {
        Person a = (Person)path.elementAt(i);
        Person b = (Person)path.elementAt(i+1);
        line(a.X,a.Y,b.X,b.Y);
      }
      colorMode(RGB,255);
    }


    if (maxlinks>0) {
      for (int i=0;i<FRIENDS.size();i++) {
        if ( !path.contains((Person)FRIENDS.elementAt(i)) ) {
          Person friend = (Person)FRIENDS.elementAt(i);
          if (friend.areYouOrDoYouKnow(target,maxlinks-1,path,exact_number)) {
            out = true;
          }
        }
      }
    }
    
    return out;
  }

  void drawDot(boolean active) {
    if (active) stroke(0); 
    else noStroke();
    fill(255,0,0);
    ellipse(X,Y,2,2);
  }

  void drawName() {
    fill(255,255,0);
    text(NAME,(int)X+3,(int)Y-3);
  }

  void drawFriends() {
    stroke(0,192,0,128);
    for (int i=0;i<FRIENDS.size();i++) {
      Person f = (Person)FRIENDS.elementAt(i);
      line(X,Y,f.X,f.Y);
    }
  }

}
