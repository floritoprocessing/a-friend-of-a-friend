class Group extends Vector {

  Person source, target;
  SpaceGrid spaceGrid;
  int WIDTH, HEIGHT;

  Group(int w, int h) {
    super();
    WIDTH = w;
    HEIGHT = h;
    spaceGrid = new SpaceGrid(WIDTH,HEIGHT,20);
  }

  boolean add(Object o) {
    super.add(o);
    spaceGrid.addPersonToBox((Person)o);
    return true;
  }

  //
  //  METHODS: Person
  //

  Person getPersonAt(int i) {
    return (Person)elementAt(i);
  }

  Person getPersonNamed(String n) {
    for (int i=0;i<size();i++) {
      if (((Person)elementAt(i)).NAME.equals(n)) return ((Person)elementAt(i));
    }
    return null;
  }

  Person getPersonClosestToPos(int x, int y) {
    float minDistSq=9999*9999;
    int closestI=-1;
    for (int i=0;i<size();i++) {
      Person p = getPersonAt(i);
      float dx = x-p.X;
      float dy = y-p.Y;
      float distSq = dx*dx+dy*dy;
      if (distSq<minDistSq) {
        minDistSq=distSq;
        closestI=i;
      }
    }
    Person p = getPersonAt(closestI);
    return p;
  }

  //
  //  METHODS: Creating Random People and Friends
  //

  void createRandomPeople(NameList nameList, int n) {
    float minDistSq = 6*6;
    while (size()<n) {
      Person p = new Person(this,nameList.getAvailableName());
      boolean overlap=true;
      while (overlap) {
        p.X=random(WIDTH);
        p.Y=random(HEIGHT);
        overlap=false;
        for (int i=0;i<size();i++) {
          Person q = getPersonAt(i);
          float dx = p.X-q.X, dy = p.Y-q.Y;
          if (dx*dx+dy*dy<minDistSq) overlap=true;
        }
      }
      add(p);
    }
  }

  void makeRandomFriends(int n) {
    for (int i=0;i<size();i++) {
      Person A = getPersonAt(i);
      Person B = getPersonAt(i);
      for (int f=0;f<n;f++) {
        int j=i;
        while (j==i) j=(int)random(size());
        B = getPersonAt(j);
        if (A.FRIENDS.size()<NR_OF_FRIENDS&&B.FRIENDS.size()<n)  {
          boolean success = A.makeFriendship(B.NAME);
          if (!success) f--;
        }
      }
    }
  }

  void makeRandomPerson(String name) {//, int n) {
    Person p = new Person(this,name);
    p.X = WIDTH/2.0;
    p.Y = HEIGHT/2.0;
    add(p);
    /*
    for (int i=0;i<n;i++) {
     int j=(int)random(group.size());
     Person B = getPersonAt(j);
     while (p.equals(B)&&B.FRIENDS.size()>=n) {
     j=(int)random(group.size());
     B = getPersonAt(j);
     }
     p.makeFriendship(B.NAME);
     }
     */
  }

  void makeForeignFriends(float chance) {
    for (int i=0;i<group.size();i++) {
      if (random(1.0)<chance) {
        int j=i;
        while (j==i) j=(int)random(group.size());
        group.getPersonAt(i).makeFriendship(group.getPersonAt(j).NAME);
      }
    }
  }

  //
  //  METHOD: make friends based on SpaceGrid
  //

  void makeCloseFriends(int n) {
    for (int i=0;i<size();i++) {

      Person p = getPersonAt(i);
      // get neighbours: 0 and 1 area

      boolean enoughFriends = false;
      Vector people = (Vector)(spaceGrid.getPeopleAroundPerson(p)).clone();
      int area = 1;

      while (!enoughFriends&&area<10) {
        // add more neighbours
        people.addAll((Vector)(spaceGrid.getPeopleAroundPerson(p,area)).clone());
        // shuffle neighbours:
        Vector peopleShuffled = new Vector();
        while (people.size()>0) {
          int ii = (int)random(people.size());
          peopleShuffled.add(people.elementAt(ii));
          people.removeElementAt(ii);
        }
        // find friends
        int ii=0;
        while (p.FRIENDS.size()<n&&ii<peopleShuffled.size()) {
          Person friend = (Person)peopleShuffled.elementAt(ii);
          if (friend.FRIENDS.size()<n) {
            p.makeFriendship(friend.NAME);
          }
          ii++;
        }
        if (p.FRIENDS.size()>=n) enoughFriends = true;
        area++;
      }

    }
  }

  void setTargetPerson(Person p) {
    target=p;
  }

  void setSourcePerson(Person p) {
    source=p;
  }

  void showSourceToTarget(int maxlinks, boolean exact_number) {
    if (!source.equals(target))
      source.areYouOrDoYouKnow(target,maxlinks-1,new Vector(),exact_number);
  }



  void draw() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).drawDot(false);
    source.drawDot(true);
    target.drawDot(true);
    source.drawName();
    target.drawName();
  }

}
