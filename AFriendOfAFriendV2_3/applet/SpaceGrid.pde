class SpaceGrid {
  
  int WIDTH, HEIGHT;
  float BOXSIZE;
  boolean SHOW = true;
  Vector[][] box;
  
  SpaceGrid(int w, int h, float squareSize) {
    WIDTH = (int)(w/squareSize)+1;
    HEIGHT = (int)(h/squareSize)+1;
    BOXSIZE = squareSize;
    init();
  }
  
  void init() {
    box = new Vector[WIDTH][HEIGHT];
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) box[x][y] = new Vector();
  }
  
  void clear() {
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) box[x][y].clear();
  }
  
  void showBoxAt(int x, int y) {
    stroke(128,128,255);
    noFill();
    rectMode(CORNER);
    rect(x*BOXSIZE,y*BOXSIZE,BOXSIZE,BOXSIZE);
  }
  
  void addPersonToBox(Person p) {
    getPeopleAroundPerson(p).add(p);
  }
  
  Vector getPeopleAroundPerson(Person p) {
    int x = (int)(p.X/BOXSIZE);
    int y = (int)(p.Y/BOXSIZE);
    if (SHOW) showBoxAt(x,y);
    Vector out = box[x][y];
    out.remove(p);
    return out;
  }
  
  Vector getPeopleAroundPerson(Person p, int offset) 
  // GETS boxes around person p. example offset 2 returns these boxes:
  //  * * * * *
  //  * . . . *
  //  * . p . *
  //  * . . . *
  //  * * * * *
  {
    int xc=(int)(p.X/BOXSIZE);
    int yc=(int)(p.Y/BOXSIZE);
    int x0=xc-offset, x1=xc+offset;
    int y0=yc-offset, y1=yc+offset;
    if (offset<0) return null;
    else if (offset==0) {
      return getPeopleAroundPerson(p);
    }
    Vector out = new Vector();
    // first line:
    if (y0>=0) for (int x=x0;x<=x1;x++) if (x>=0&&x<WIDTH) {
      if (SHOW) showBoxAt(x,y0);
      out.addAll(box[x][y0]);
    }
    // middle lines:
    for (int y=y0+1;y<=y1-1;y++) if (y>=0&&y<HEIGHT) for (int x=x0;x<=x1;x+=(x1-x0)) if (x>=0&&x<WIDTH) {
      if (SHOW) showBoxAt(x,y);
      out.addAll(box[x][y]);
    }
    // last line:
    if (y1<HEIGHT) for (int x=x0;x<=x1;x++) if (x>=0&&x<WIDTH) {
      if (SHOW) showBoxAt(x,y1);
      out.addAll(box[x][y1]);
    }
    return out;
  }
  
  
  
  
}
