import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.*; 
import java.awt.image.*; 
import java.awt.event.*; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class AFriendOfAFriendV2_3 extends PApplet {

int NR_OF_PEOPLE = 1000;
int NR_OF_FRIENDS = 6;

Group group;
PFont font;


public void setup() {
  size(640,480,P3D);
  ellipseMode(CENTER_RADIUS);

  font = loadFont("Aldine401BT-BoldA-15.vlw");
  textFont(font,15);

  group = new Group(width,height);

  NameList nl = new NameList();
  //nl.reduceTo(500);

  // CREATE PEOPLE:
  group.createRandomPeople(nl,NR_OF_PEOPLE);

  // MAKE FRIENDS:
  // group.makeRandomFriends(NR_OF_FRIENDS);
  
  // create random person
  group.makeRandomPerson("Marcus");//,0);//NR_OF_FRIENDS);
  
  group.makeForeignFriends(0.05f);
  
  group.makeCloseFriends(NR_OF_FRIENDS);
  
}

public void mousePressed() {
  setup();
}

public void draw() {
  colorMode(RGB,255);
  background(0);
    
  group.setSourcePerson(group.getPersonClosestToPos(mouseX,mouseY));
  //group.setSourcePerson(group.getPersonAt((int)((group.size()-2)*((float)mouseX/width))));
  group.setTargetPerson(group.getPersonNamed("Marcus"));
  group.draw();
  
  (group.source).drawFriends();
  (group.target).drawFriends();
    
  stroke(128,128,128);
  group.showSourceToTarget(7,false);  
  
//  Vector b = group.spaceGrid.getPeopleAroundPerson(group.getPersonNamed("Marcus"),3);
  //println(b.size());
//  Vector c = group.spaceGrid.getPeopleAroundPerson(group.getPersonNamed("Marcus"));
  //println(c.size());
}
class Group extends Vector {

  Person source, target;
  SpaceGrid spaceGrid;
  int WIDTH, HEIGHT;

  Group(int w, int h) {
    super();
    WIDTH = w;
    HEIGHT = h;
    spaceGrid = new SpaceGrid(WIDTH,HEIGHT,20);
  }

  public boolean add(Object o) {
    super.add(o);
    spaceGrid.addPersonToBox((Person)o);
    return true;
  }

  //
  //  METHODS: Person
  //

  public Person getPersonAt(int i) {
    return (Person)elementAt(i);
  }

  public Person getPersonNamed(String n) {
    for (int i=0;i<size();i++) {
      if (((Person)elementAt(i)).NAME.equals(n)) return ((Person)elementAt(i));
    }
    return null;
  }

  public Person getPersonClosestToPos(int x, int y) {
    float minDistSq=9999*9999;
    int closestI=-1;
    for (int i=0;i<size();i++) {
      Person p = getPersonAt(i);
      float dx = x-p.X;
      float dy = y-p.Y;
      float distSq = dx*dx+dy*dy;
      if (distSq<minDistSq) {
        minDistSq=distSq;
        closestI=i;
      }
    }
    Person p = getPersonAt(closestI);
    return p;
  }

  //
  //  METHODS: Creating Random People and Friends
  //

  public void createRandomPeople(NameList nameList, int n) {
    float minDistSq = 6*6;
    while (size()<n) {
      Person p = new Person(this,nameList.getAvailableName());
      boolean overlap=true;
      while (overlap) {
        p.X=random(WIDTH);
        p.Y=random(HEIGHT);
        overlap=false;
        for (int i=0;i<size();i++) {
          Person q = getPersonAt(i);
          float dx = p.X-q.X, dy = p.Y-q.Y;
          if (dx*dx+dy*dy<minDistSq) overlap=true;
        }
      }
      add(p);
    }
  }

  public void makeRandomFriends(int n) {
    for (int i=0;i<size();i++) {
      Person A = getPersonAt(i);
      Person B = getPersonAt(i);
      for (int f=0;f<n;f++) {
        int j=i;
        while (j==i) j=(int)random(size());
        B = getPersonAt(j);
        if (A.FRIENDS.size()<NR_OF_FRIENDS&&B.FRIENDS.size()<n)  {
          boolean success = A.makeFriendship(B.NAME);
          if (!success) f--;
        }
      }
    }
  }

  public void makeRandomPerson(String name) {//, int n) {
    Person p = new Person(this,name);
    p.X = WIDTH/2.0f;
    p.Y = HEIGHT/2.0f;
    add(p);
    /*
    for (int i=0;i<n;i++) {
     int j=(int)random(group.size());
     Person B = getPersonAt(j);
     while (p.equals(B)&&B.FRIENDS.size()>=n) {
     j=(int)random(group.size());
     B = getPersonAt(j);
     }
     p.makeFriendship(B.NAME);
     }
     */
  }

  public void makeForeignFriends(float chance) {
    for (int i=0;i<group.size();i++) {
      if (random(1.0f)<chance) {
        int j=i;
        while (j==i) j=(int)random(group.size());
        group.getPersonAt(i).makeFriendship(group.getPersonAt(j).NAME);
      }
    }
  }

  //
  //  METHOD: make friends based on SpaceGrid
  //

  public void makeCloseFriends(int n) {
    for (int i=0;i<size();i++) {

      Person p = getPersonAt(i);
      // get neighbours: 0 and 1 area

      boolean enoughFriends = false;
      Vector people = (Vector)(spaceGrid.getPeopleAroundPerson(p)).clone();
      int area = 1;

      while (!enoughFriends&&area<10) {
        // add more neighbours
        people.addAll((Vector)(spaceGrid.getPeopleAroundPerson(p,area)).clone());
        // shuffle neighbours:
        Vector peopleShuffled = new Vector();
        while (people.size()>0) {
          int ii = (int)random(people.size());
          peopleShuffled.add(people.elementAt(ii));
          people.removeElementAt(ii);
        }
        // find friends
        int ii=0;
        while (p.FRIENDS.size()<n&&ii<peopleShuffled.size()) {
          Person friend = (Person)peopleShuffled.elementAt(ii);
          if (friend.FRIENDS.size()<n) {
            p.makeFriendship(friend.NAME);
          }
          ii++;
        }
        if (p.FRIENDS.size()>=n) enoughFriends = true;
        area++;
      }

    }
  }

  public void setTargetPerson(Person p) {
    target=p;
  }

  public void setSourcePerson(Person p) {
    source=p;
  }

  public void showSourceToTarget(int maxlinks, boolean exact_number) {
    if (!source.equals(target))
      source.areYouOrDoYouKnow(target,maxlinks-1,new Vector(),exact_number);
  }



  public void draw() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).drawDot(false);
    source.drawDot(true);
    target.drawDot(true);
    source.drawName();
    target.drawName();
  }

}
class NameList {
  
  String[] NAMES;
  boolean[] used;
  
  NameList() {
    print("Loading names: ");
    NAMES = loadStrings("names.TXT");
    int a = NAMES.length;
    println(a+" names.");
    used = new boolean[a];
    for (int i=0; i<a; i++) used[i]=false;
  }
  
  public void reduceTo(int a) {
    String[] newNames = new String[a];
    for (int i=0;i<newNames.length;i++) newNames[i]=NAMES[i];
    NAMES = new String[a];
    for (int i=0;i<newNames.length;i++) NAMES[i]=newNames[i];
    println("Namelist reduced to "+NAMES.length+" names.");
  }
  
  public String getAnyName() {
    int n = (int)random(NAMES.length);
    used[n] = true;
    return NAMES[n];
  }
  
  public String getAvailableName() {
    boolean found=false;
    int n=0;
    while (!found) {
      n=(int)random(NAMES.length);
      if (!used[n]) {
        used[n]=true;
        found=true;
      }
    }
    return NAMES[n];
  }
  
}
class Person {

  String NAME;
  Vector FRIENDS = new Vector();
  float X, Y;

  Group group;

  Person(Group _group, String n) {
    group = _group;
    NAME = n;
    X = random(width);
    Y = random(height);
  }
  
  public boolean makeFriendship(String n) {
    Person p = group.getPersonNamed(n);
    if (p==this) return false;
    if (p==null) {
      //println("creating new person as friend");
      p = new Person(group,n);
      group.add(p);
      float rd = random(TWO_PI);
      float r = random(20,50);
      p.X = X + r*cos(rd);
      p.Y = Y + r*sin(rd);
      if (p.X<0) p.X=0;
      if (p.X>width) p.X=width;
      if (p.Y<0) p.Y=0;
      if (p.Y>height) p.Y=height;
    } 
    else {
      //println("linking to existing person");
    }
    if (FRIENDS.contains(p)) {
      //println("friendship already exists!");
      return false;
    }
    FRIENDS.add(p);
    p.FRIENDS.add(this);
    return true;
  }


  public boolean areYouOrDoYouKnow(Person target, int maxlinks, Vector _path, boolean exact_number) {

    if (_path.contains(this)) return false;
    
    Vector path = new Vector();
    for (int i=0;i<_path.size();i++) path.add(_path.elementAt(i));
    path.add(this);

    boolean out = false;
    
    if (exact_number) {
      if (maxlinks==0) {
        if (target.equals(this)) out = true;
      }
    } 
    else {
      if (target.equals(this)) {
        out = true;
      }
    }
    
    // if final elemen of path: draw it!
    if (out==true) {
      
      // make unique color from path:
      int c = 0;
      for (int i=0;i<path.size();i++) c+=group.indexOf(path.elementAt(i));
      c = c%0xFF;
      colorMode(HSB,255);
      stroke(c,255,255,128);
      for (int i=0;i<path.size()-1;i++) {
        Person a = (Person)path.elementAt(i);
        Person b = (Person)path.elementAt(i+1);
        line(a.X,a.Y,b.X,b.Y);
      }
      colorMode(RGB,255);
    }


    if (maxlinks>0) {
      for (int i=0;i<FRIENDS.size();i++) {
        if ( !path.contains((Person)FRIENDS.elementAt(i)) ) {
          Person friend = (Person)FRIENDS.elementAt(i);
          if (friend.areYouOrDoYouKnow(target,maxlinks-1,path,exact_number)) {
            out = true;
          }
        }
      }
    }
    
    return out;
  }

  public void drawDot(boolean active) {
    if (active) stroke(0); 
    else noStroke();
    fill(255,0,0);
    ellipse(X,Y,2,2);
  }

  public void drawName() {
    fill(255,255,0);
    text(NAME,(int)X+3,(int)Y-3);
  }

  public void drawFriends() {
    stroke(0,192,0,128);
    for (int i=0;i<FRIENDS.size();i++) {
      Person f = (Person)FRIENDS.elementAt(i);
      line(X,Y,f.X,f.Y);
    }
  }

}
class SpaceGrid {
  
  int WIDTH, HEIGHT;
  float BOXSIZE;
  boolean SHOW = true;
  Vector[][] box;
  
  SpaceGrid(int w, int h, float squareSize) {
    WIDTH = (int)(w/squareSize)+1;
    HEIGHT = (int)(h/squareSize)+1;
    BOXSIZE = squareSize;
    init();
  }
  
  public void init() {
    box = new Vector[WIDTH][HEIGHT];
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) box[x][y] = new Vector();
  }
  
  public void clear() {
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) box[x][y].clear();
  }
  
  public void showBoxAt(int x, int y) {
    stroke(128,128,255);
    noFill();
    rectMode(CORNER);
    rect(x*BOXSIZE,y*BOXSIZE,BOXSIZE,BOXSIZE);
  }
  
  public void addPersonToBox(Person p) {
    getPeopleAroundPerson(p).add(p);
  }
  
  public Vector getPeopleAroundPerson(Person p) {
    int x = (int)(p.X/BOXSIZE);
    int y = (int)(p.Y/BOXSIZE);
    if (SHOW) showBoxAt(x,y);
    Vector out = box[x][y];
    out.remove(p);
    return out;
  }
  
  public Vector getPeopleAroundPerson(Person p, int offset) 
  // GETS boxes around person p. example offset 2 returns these boxes:
  //  * * * * *
  //  * . . . *
  //  * . p . *
  //  * . . . *
  //  * * * * *
  {
    int xc=(int)(p.X/BOXSIZE);
    int yc=(int)(p.Y/BOXSIZE);
    int x0=xc-offset, x1=xc+offset;
    int y0=yc-offset, y1=yc+offset;
    if (offset<0) return null;
    else if (offset==0) {
      return getPeopleAroundPerson(p);
    }
    Vector out = new Vector();
    // first line:
    if (y0>=0) for (int x=x0;x<=x1;x++) if (x>=0&&x<WIDTH) {
      if (SHOW) showBoxAt(x,y0);
      out.addAll(box[x][y0]);
    }
    // middle lines:
    for (int y=y0+1;y<=y1-1;y++) if (y>=0&&y<HEIGHT) for (int x=x0;x<=x1;x+=(x1-x0)) if (x>=0&&x<WIDTH) {
      if (SHOW) showBoxAt(x,y);
      out.addAll(box[x][y]);
    }
    // last line:
    if (y1<HEIGHT) for (int x=x0;x<=x1;x++) if (x>=0&&x<WIDTH) {
      if (SHOW) showBoxAt(x,y1);
      out.addAll(box[x][y1]);
    }
    return out;
  }
  
  
  
  
}
/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  double x=0,y=0,z=0;
  
  Vec() {
  }
  
  Vec(double _x, double _y) {
    x=_x;
    y=_y;
  }
  
  Vec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  Vec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  public void setVec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  public void setVec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  public double len() {
    return Math.sqrt(x*x+y*y+z*z);
  }
  
  public double lenSQ() {
    return (x*x+y*y+z*z);
  }
  
  public void gravitateTo(Vec _to, double _mgd, double _fFac) {
    double minGravDist2=_mgd*_mgd;
    double dx=_to.x-x;
    double dy=_to.y-y;
    double dz=_to.z-z;
    double d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {d2=minGravDist2;}
    //double d=Math.sqrt(d2);
    double F=_fFac/d2;
    add(new Vec(dx*F,dy*F,dz*F));
  }

  public void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }
  
  public void add(double _x, double _y, double _z) {
    x+=_x;
    y+=_y;
    z+=_z;
  }

  public void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  public void mul(double p) {
    x*=p;
    y*=p;
    z*=p;
  }
  
  public void div(double p) {
    x/=p;
    y/=p;
    z/=p;
  }

  public void negX() {
    x=-x;
  }

  public void negY() {
    y=-y;
  }

  public void negZ() {
    z=-z;
  }

  public void neg() {
    negX();
    negY();
    negZ();
  }
  
  public void normalize() {
    double l=len();
    if (l!=0) { // if not nullvector
      div(l);
    }
  }
  
  public Vec getNormalized() {
    double l=len();
    if (l!=0) {
      Vec out=new Vec(this);
      out.div(l);
      return out;
    } else {
      return new Vec();
    }
  }
  
  public void setLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/len();
    x*=fac;
    y*=fac;
    z*=fac;
  }
  
  public void toAvLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/((ml+len())/2.0f);
    x*=fac;
    y*=fac;
    z*=fac;
  }

  public void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double yn=y*COS-z*SIN;
    double zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  public void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-z*SIN; 
    double zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  public void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-y*SIN; 
    double yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  public void rot(double xrd, double yrd, double zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }
  
  public boolean isNullVec() {
    if (x==0&&y==0&&z==0) {return true;} else {return false;}
  }
}




public Vec vecAdd(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.add(b);
  return out;
}

public Vec vecSub(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.sub(b);
  return out;
}

public Vec vecMul(Vec a, double b) {
  Vec out=new Vec(a);
  out.mul(b);
  return out;
}

public Vec vecDiv(Vec a, double b) {
  Vec out=new Vec(a);
  out.div(b);
  return out;
}

public double vecLen(Vec a) {
  return a.len();
}

public Vec vecRotY(Vec a, double rd) {
  Vec out=new Vec(a);
  out.rotY(rd);
  return out;
}

public Vec rndDirVec(double r) {
  Vec out=new Vec(0,0,r);
  out.rotX(random(-PI/2.0f,PI));
  out.rotZ(random(0,2*PI));
  return out;
}

public Vec rndPlusMinVec(double s) {
  Vec out=new Vec(-s+2*s*Math.random(),-s+2*s*Math.random(),-s+2*s*Math.random());
  return out;
}

public void vecSwap(Vec v1, Vec v2) {
  Vec t=new Vec(v1);
  v1.setVec(v2);
  v2.setVec(t);
}

/*****************************
 * adapted 3d functions to Vec
 *****************************/

public void vecTranslate(Vec v) {
  translate((float)v.x,(float)v.y,(float)v.z);
}

public void vecVertex(Vec v) {
  vertex((float)v.x,(float)v.y,(float)v.z);
}

public void vecLine(Vec a, Vec b) {
  line((float)a.x,(float)a.y,(float)a.z,(float)b.x,(float)b.y,(float)b.z);
}

public void vecRect(Vec a, Vec b) {
  rect((float)a.x,(float)a.y,(float)b.x,(float)b.y);
}

public void vecCamera(Vec eye, Vec cen, Vec upaxis) {
  camera((float)eye.x,(float)eye.y,(float)eye.z,(float)cen.x,(float)cen.y,(float)cen.z,(float)upaxis.x,(float)upaxis.y,(float)upaxis.z);
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#ece9d8", "AFriendOfAFriendV2_3" });
  }
}
