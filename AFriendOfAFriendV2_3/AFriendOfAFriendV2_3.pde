int NR_OF_PEOPLE = 1000;
int NR_OF_FRIENDS = 6;

Group group;
PFont font;


void setup() {
  size(640,480,P3D);
  ellipseMode(CENTER);

  font = loadFont("Aldine401BT-BoldA-15.vlw");
  textFont(font,15);

  group = new Group(width,height);

  NameList nl = new NameList();
  //nl.reduceTo(500);

  // CREATE PEOPLE:
  group.createRandomPeople(nl,NR_OF_PEOPLE);

  // MAKE FRIENDS:
  // group.makeRandomFriends(NR_OF_FRIENDS);
  
  // create random person
  group.makeRandomPerson("Marcus");//,0);//NR_OF_FRIENDS);
  
  group.makeForeignFriends(0.05);
  
  group.makeCloseFriends(NR_OF_FRIENDS);
  
}

void mousePressed() {
  setup();
}

void draw() {
  colorMode(RGB,255);
  background(0);
    
  group.setSourcePerson(group.getPersonClosestToPos(mouseX,mouseY));
  //group.setSourcePerson(group.getPersonAt((int)((group.size()-2)*((float)mouseX/width))));
  group.setTargetPerson(group.getPersonNamed("Marcus"));
  group.draw();
  
  (group.source).drawFriends();
  (group.target).drawFriends();
    
  stroke(128,128,128);
  group.showSourceToTarget(7,false);  
  
//  Vector b = group.spaceGrid.getPeopleAroundPerson(group.getPersonNamed("Marcus"),3);
  //println(b.size());
//  Vector c = group.spaceGrid.getPeopleAroundPerson(group.getPersonNamed("Marcus"));
  //println(c.size());
}
