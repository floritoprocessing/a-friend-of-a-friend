class NameList {
  
  String[] NAMES;
  boolean[] used;
  
  NameList() {
    print("Loading names: ");
    NAMES = loadStrings("names.TXT");
    int a = NAMES.length;
    println(a+" names.");
    used = new boolean[a];
    for (int i=0; i<a; i++) used[i]=false;
  }
  
  void reduceTo(int a) {
    String[] newNames = new String[a];
    for (int i=0;i<newNames.length;i++) newNames[i]=NAMES[i];
    NAMES = new String[a];
    for (int i=0;i<newNames.length;i++) NAMES[i]=newNames[i];
    println("Namelist reduced to "+NAMES.length+" names.");
  }
  
  String getAnyName() {
    int n = (int)random(NAMES.length);
    used[n] = true;
    return NAMES[n];
  }
  
  String getAvailableName() {
    boolean found=false;
    int n=0;
    while (!found) {
      n=(int)random(NAMES.length);
      if (!used[n]) {
        used[n]=true;
        found=true;
      }
    }
    return NAMES[n];
  }
  
}
