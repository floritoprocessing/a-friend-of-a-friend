class Person {

  float X=0, Y=0;
  String NAME="";
  int UNIQUE_ID=-1;
  Vector FRIENDS = new Vector();

  public static final float DRAW_SIZE = 2;
  float mouseSizeSq = 2*2;

  public static final int LINKS_SHOW_ALL = 0;
  public static final int LINKS_SHOW_FIRST = 1;
  public int LINKS_SHOW = LINKS_SHOW_FIRST;

  Person() {
  }

  Person(String name) {
    NAME = name;
  }

  Person(String name, float x, float y, int id) {
    this(name);
    X = x;
    Y = y;
    UNIQUE_ID = id;
  }
  
  Person(String name, float x, float y, int id, Vector friends) {
    this(name,x,y,id);
    FRIENDS.addAll(friends);
  }

  void draw() {
    fill(128,128,128);
    ellipse(X,Y,DRAW_SIZE,DRAW_SIZE);
  }

  boolean mouseWithinRange() {
    return (sq(mouseX-X)+sq(mouseY-Y)<mouseSizeSq);
  }

  void showName() {
    colorMode(RGB,255);
    fill(0,0,0,64);
    text(NAME,(int)X+2,(int)Y+2);
    fill(128,64,0);
    text(NAME,(int)X,(int)Y);
    colorMode(HSB,255);
  }

  void drawNameAndFriends() {
    colorMode(RGB,255);
    stroke(255,255,255,64);
    for (int i=0;i<FRIENDS.size();i++) line(X,Y,((Person)FRIENDS.elementAt(i)).X,((Person)FRIENDS.elementAt(i)).Y);
    noStroke();
    showName();
  }

  boolean startSearchFor(Person fred, int maxDepth, color drawColor) {
    Person[] visited = new Person[1];
    visited[0]=this;
    boolean directFriendFound = false;
    for (int f=0;f<FRIENDS.size();f++) {
      Person friend = (Person)FRIENDS.elementAt(f);
      if (friend.areYou(fred)) {
        stroke(drawColor);
        line(X,Y,friend.X,friend.Y);
        directFriendFound=true;
        if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
      }
    }
    if (!directFriendFound) {
      for (int f=0;f<FRIENDS.size();f++) {
        Person friend = (Person)FRIENDS.elementAt(f);
        if (friend.doYouKnow(fred,maxDepth-1,visited,morphColor(drawColor))) {
          stroke(drawColor);
          line(X,Y,friend.X,friend.Y);
          friend.showName();
          if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
        }
      }
    }
    return false;
  }

  boolean areYou(Person fred) {
    return (this.equals(fred));
  }

  boolean doYouKnow(Person fred, int maxDepth, Person[] _visited, color drawColor) {
    // create a new visited list (visited People + this)
    Person[] visited = new Person[_visited.length+1];
    for (int i=0;i<_visited.length;i++) visited[i]=_visited[i];
    visited[visited.length-1]=this;
    
    boolean answer = false;
    boolean directFriendFound = false;
    
    // is one of my friends the target?
    for (int f=0;f<FRIENDS.size();f++) {
      Person friend = (Person)FRIENDS.elementAt(f);
      
      boolean friendVisited=false;
      int v=0;
      while (v<visited.length&&!friendVisited) friendVisited=visited[v++].equals(friend); 

      if (maxDepth>=0&&!friendVisited&&friend.areYou(fred)) {
        stroke(drawColor);
        line(X,Y,friend.X,friend.Y);
        answer = true;
        directFriendFound = true;
        if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
      } 
    }
    
    // if not direct friend, is a friend of a friend the target?
    if (!directFriendFound) {
      for (int f=0;f<FRIENDS.size();f++) {
        Person friend = (Person)FRIENDS.elementAt(f);
        
        boolean friendVisited=false;
        int v=0;
        while (v<visited.length&&!friendVisited) friendVisited=visited[v++].equals(friend); 
        
        if (maxDepth>0&&!friendVisited&&friend.doYouKnow(fred,maxDepth-1,visited,morphColor(drawColor))) {
          stroke(drawColor);
          line(X,Y,friend.X,friend.Y);
          friend.showName();
          answer = true;
          if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
        }
      }
    }
    return answer;
  }

}
