int NR_OF_PEOPLE = 120;
int NR_OF_FRIENDS = 5;

Group group;
PFont font;


void setup() {
  size(640,480,P3D);
  ellipseMode(CENTER);
  
  font = loadFont("Aldine401BT-BoldA-15.vlw");
  textFont(font,15);
  
  group = new Group();
  
  NameList nl = new NameList();
  nl.reduceTo(120);
  
  // MAKE NEW PEOPLE
  while (group.size()<NR_OF_PEOPLE) {
    Person p = new Person(group,nl.getAvailableName());
    group.add(p);
    for (int i=0;i<NR_OF_FRIENDS;i++) {
      if (group.size()<NR_OF_PEOPLE) {
        boolean success = false;
        while (!success) success = p.addFriend(nl.getAnyName());
      }
    }
  }
  
  // MAKE MORE FRIENDSHIPS:
  for (int i=0;i<group.size();i++) {
    Person p = (Person)group.elementAt(i);
    if (p.FRIENDS.size()<3) {
      int j=i;
      while (i==j) j=(int)random(group.size());
      String fName=((Person)group.elementAt(j)).NAME;
      boolean success = false;
      while (!success) success = p.addFriend(fName);
    }
  }
  
}

void mousePressed() {
  setup();
}

void draw() {
  background(255);
  group.activatePersonByMouse();
  group.draw();
}
