import java.util.Vector;
class Group extends Vector {
  
  Group() {
    super();
  }
  
  Person getPersonNamed(String n) {
    for (int i=0;i<size();i++) {
      if (((Person)elementAt(i)).NAME.equals(n)) return ((Person)elementAt(i));
    }
    return null;
  }
  
  void activatePersonByMouse() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).activatePersonByMouse();
  }
  
  void draw() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).drawDot();
    for (int i=0;i<size();i++) ((Person)elementAt(i)).drawName();
    for (int i=0;i<size();i++) ((Person)elementAt(i)).drawFriends();
  }
  
}
