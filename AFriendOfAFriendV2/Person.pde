class Person {
  
  String NAME;
  Vector FRIENDS = new Vector();
  float X, Y;
  
  boolean active = false;
  Group group;
  
  Person(Group _group, String n) {
    group = _group;
    NAME = n;
    X = random(width);
    Y = random(height);
  }
  
  boolean addFriend(String n) {
    Person p = group.getPersonNamed(n);
    if (p==null) {
      //println("creating new person as friend");
      p = new Person(group,n);
      float rd = random(TWO_PI);
      float r = random(20,50);
      p.X = X + r*cos(rd);
      p.Y = Y + r*sin(rd);
      if (p.X<0) p.X=0;
      if (p.X>width) p.X=width;
      if (p.Y<0) p.Y=0;
      if (p.Y>height) p.Y=height;
    } else {
      //println("linking to existing person");
    }
    if (FRIENDS.contains(p)) return false;
    FRIENDS.add(p);
    p.FRIENDS.add(this);
    group.add(p);
    return true;
  }
  
  void activatePersonByMouse() {
    float dx = mouseX-X;
    float dy = mouseY-Y;
    if (dx*dx+dy*dy<25) active = true; else active = false;
  }
  
  void drawDot() {
    if (active) stroke(0); else noStroke();
    fill(255,0,0);
    ellipse(X,Y,5,5);
  }
  
  void drawName() {
    if (active) {
      fill(0);
      text(NAME,(int)X+3,(int)Y-3);
    }
  }
  
  void drawFriends() {
    if (active) {
      stroke(0,0,0,128);
      for (int i=0;i<FRIENDS.size();i++) {
        Person f = (Person)FRIENDS.elementAt(i);
        line(X,Y,f.X,f.Y);
      }
    }
  }
  
}
