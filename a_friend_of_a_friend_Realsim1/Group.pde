import java.util.Vector;
class Group extends Vector {

  //float FRIEND_DISTANCE_DIVIDER = 1;
  String[] boyName = {
    "Marcus","Peter","Olaf","Wart","Dennis","Arjen","Floris"
  };
  
  String[] girlName = {
    "Andrea","Stev","Tamara","Wing","Jessica"
  };


  int INDEX_OF_MOUSEOVER=-1;

  Group() {
    super();
  }

  void addPerson(Object o) {
    
    if (nameExistsInGroup(((Person)o).NAME)) {
      println("Will not add "+((Person)o).NAME+", already existing!");
    } else {
      add(o);
    }
  }
  
  Person person(int i) {
    return ((Person)elementAt(i));
  }
  
  Person personNamed(String name) {
    return person(indexOfPersonNamed(name));
  }
  
  int indexOfPersonNamed(String name) {
    for (int i=0;i<size();i++) if (person(i).NAME.equals(name)) return i;
    throw new Error("person named "+name+" not found!");
  }

  boolean nameExistsInGroup(String name) {
    for (int i=0;i<size();i++) if (person(i).NAME.equals(name)) return true;
    return false;
  }

  void initNames() {
    // load names
    // http://www.listofbabynames.org/k_girls.htm
    boyName = loadStrings("boys.TXT");
    girlName = loadStrings("girls.TXT");
    println(boyName.length+" boy names and "+girlName.length+" girl names");
  }
  
  void reduceNamesTo(int amount) {
    String[] newBoyName = new String[amount/2];
    String[] newGirlName = new String[amount-newBoyName.length];
    for (int i=0;i<newBoyName.length;i++) newBoyName[i]=boyName[i];
    for (int i=0;i<newGirlName.length;i++) newGirlName[i]=girlName[i];
    boyName = new String[newBoyName.length];
    girlName = new String[newGirlName.length];
    for (int i=0;i<newBoyName.length;i++) boyName[i]=newBoyName[i];
    for (int i=0;i<newGirlName.length;i++) girlName[i]=newGirlName[i];
  }

  boolean spotIsEmpty(float x, float y) {
    float minDSq=sq(2*Person.DRAW_SIZE);
    for (int t=0;t<size();t++) {
      if (sq(person(t).X-x)+sq(person(t).Y-y)<minDSq) {
        return false;
      }
    }
    return true;
  }

  float[] createEmptySpot() {
    boolean overlay=true;
    float x=0,y=0;
    while (overlay) {
      overlay=false;
      x=width*random(0.2,0.8);
      y=height*random(0.2,0.8);
      overlay=!spotIsEmpty(x,y);
    }
    float[] out = new float[2];
    out[0]=x;
    out[1]=y;
    return out;
  }

  void create(int amount, int minF, int maxF) {
    // CREATE GROUP
    println("creating group");
    for (int i=0;i<amount;i++) {

      // no duplicate names:
      String name="";
      boolean existing=true;
      while (existing) {
        name = (random(1.0)<0.5)?boyName[(int)random(boyName.length)]:girlName[(int)random(girlName.length)];
        existing=nameExistsInGroup(name);
        //println(name+" already existing, try again");
      }

      // position:
      float[] xy = createEmptySpot();
      float x=xy[0];
      float y=xy[1];
      
      // make person:
      println("Creating "+name);
      Person person = new Person(name,x,y);


      // make friends:
      int amountOfFriends = (int)random(minF,maxF);
      for (int f=0;f<amountOfFriends;f++) {
        String friendName = (random(1.0)<0.5)?boyName[(int)random(boyName.length)]:girlName[(int)random(girlName.length)];
        person.addFriend(group,friendName);
      }


      add(person);
    }
  }





  void setShowStatus(int ls) {
    for (int i=0;i<size();i++)
      person(i).LINKS_SHOW=ls;
  }
  void toggleShowStatus() {
    int ls = person(0).LINKS_SHOW;
    ls++;
    if (ls>Person.LINKS_SHOW_FIRST) ls=0;
    setShowStatus(ls);
  }


  void draw() {
    for (int i=0;i<size();i++) person(i).draw();
  }
  
  void hover() {
    for (int i=0;i<size();i++) person(i).hover();
  }
  
  void drawName(int i) {
    person(i).drawName();
  }
  
  void drawNameAndFriends(int i) {
    person(i).drawNameAndFriends();
  }

  void makeIndexOfMouseover() {
    INDEX_OF_MOUSEOVER=-1;
    for (int i=0;i<size();i++) {
      if (person(i).mouseWithinRange()) INDEX_OF_MOUSEOVER = i;
    }
  }

}
