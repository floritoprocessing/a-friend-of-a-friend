class Person {

  float X=0, Y=0;
  float baseX, baseY;
  
  float hoverPhase=random(TWO_PI);
  String NAME="";
  //  int UNIQUE_ID=-1;
  Vector FRIENDS = new Vector();

  public static final float DRAW_SIZE = 5;
  float mouseSizeSq = 5*5;

  public static final int LINKS_SHOW_ALL = 0;
  public static final int LINKS_SHOW_FIRST = 1;
  public int LINKS_SHOW = LINKS_SHOW_FIRST;

  Person() {
  }

  Person(String name) {
    NAME = name;
  }

  Person(String name, float x, float y) {
    this(name);
    if (x>width-1) x=2*(width-1)-x;
    if (x<0) x=-x;
    if (y>height-1) y=2*(height-1)-y;
    if (y<0) y=-y;
    baseX = x;
    baseY = y;
    X = x;
    Y = y;
    //    UNIQUE_ID = id;
  }

  Person(String name, float[] xy) {
    this(name,xy[0],xy[1]);
  }

  Person(String name, float x, float y, Vector friends) {
    this(name,x,y);
    FRIENDS.addAll(friends);
  }
  
  void hover() {
    hoverPhase+=0.02;
    if (hoverPhase>TWO_PI) hoverPhase-=TWO_PI;
    Y = baseY+3*cos(hoverPhase);
  }

  void draw() {
    fill(128,128,128);
    ellipse(X,Y,DRAW_SIZE,DRAW_SIZE);
  }

  boolean mouseWithinRange() {
    return (sq(mouseX-X)+sq(mouseY-Y)<mouseSizeSq);
  }

  void drawName() {
    showName(color(128,64,0,64));
  }

  void showName(color c) {
    colorMode(RGB,255);
    fill(0,0,0,128);
    text(NAME,(int)X+2,(int)Y+2);
    fill(c);
    text(NAME,(int)X,(int)Y);
  }

  void drawNameAndFriends() {
    colorMode(RGB,255);
    stroke(255,255,255,16);
    for (int i=0;i<FRIENDS.size();i++) line(X,Y,((Person)FRIENDS.elementAt(i)).X,((Person)FRIENDS.elementAt(i)).Y);
    noStroke();
    showName(color(128,64,0));
  }



  void addFriend(Group g, String friendName) {
    if (!g.nameExistsInGroup(friendName)) {
      // CREATE Friend & LINK
      float radius=random(50,150);
      boolean empty=false;
      float fx=0, fy=0;
      while (!empty) {
        float degree=random(TWO_PI);
        fx = X+radius*cos(degree);
        fy = Y+radius*sin(degree);
        empty = g.spotIsEmpty(fx,fy) && fx>0 && fx<width && fy>0 && fy<height;
      }
      Person friend = new Person(friendName,fx,fy);
      friend.FRIENDS.add(this);
      FRIENDS.add(friend);
      g.add(friend);
      println("... added "+friendName);

    } 
    else {
      // LINK if already existing and not yet in Friends list
      Person friend = g.personNamed(friendName);
      if (!FRIENDS.contains(friend)) {
        println("LINKING TO EXISTING FRIEND: "+friend.NAME);
        FRIENDS.add(friend);
        friend.FRIENDS.add(this);
      } else {
        println("ignoring new friend "+friend.NAME+", already linked!");
      }
    }
  }



  boolean startSearchFor(Person fred, int maxDepth) {
    Person[] visited = new Person[1];
    visited[0]=this;
    boolean directFriendFound = false;
    for (int f=0;f<FRIENDS.size();f++) {
      Person friend = (Person)FRIENDS.elementAt(f);
      if (friend.areYou(fred,visited)) {
        directFriendFound=true;
        if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
      }
    }
    if (!directFriendFound) {
      for (int f=0;f<FRIENDS.size();f++) {
        Person friend = (Person)FRIENDS.elementAt(f);
        if (friend.doYouKnow(fred,maxDepth-1,visited)) {
          if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
        }
      }
    }
    return false;
  }

  void personCurve(Person[] p, int a0, int p0, int p1, int a1) {
    if (a0<0) a0=0;
    if (a1>p.length-1) a1=p.length-1;
    curve(p[a0].X,p[a0].Y,p[p0].X,p[p0].Y,p[p1].X,p[p1].Y,p[a1].X,p[a1].Y);
  }

  boolean areYou(Person fred, Person[] _visited) {
    if (this.equals(fred)) {

      Person[] p = new Person[_visited.length+1];
      for (int i=0;i<_visited.length;i++) p[i]=_visited[i];
      p[_visited.length]=this;

      colorMode(RGB,255);
      stroke(random(128,255),random(128,255),random(128,255));
      noFill();
      for (int i=0;i<p.length-1;i++) personCurve(p,i-1,i,i+1,i+2);
      for (int i=1;i<p.length-1;i++) p[i].showName(color(255,192,66));

    }
    return (this.equals(fred));
  }


  boolean doYouKnow(Person fred, int maxDepth, Person[] _visited) {
    // create a new visited list (visited People + this)
    Person[] visited = new Person[_visited.length+1];
    for (int i=0;i<_visited.length;i++) visited[i]=_visited[i];
    visited[visited.length-1]=this;

    boolean answer = false;
    boolean directFriendFound = false;

    // is one of my friends the target?
    for (int f=0;f<FRIENDS.size();f++) {
      Person friend = (Person)FRIENDS.elementAt(f);

      boolean friendVisited=false;
      int v=0;
      while (v<visited.length&&!friendVisited) friendVisited=visited[v++].equals(friend); 

      if (maxDepth>=0&&!friendVisited&&friend.areYou(fred,visited)) {
        answer = true;
        directFriendFound = true;
        if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
      } 
    }

    // if not direct friend, is a friend of a friend the target?
    if (!directFriendFound) {
      for (int f=0;f<FRIENDS.size();f++) {
        Person friend = (Person)FRIENDS.elementAt(f);

        boolean friendVisited=false;
        int v=0;
        while (v<visited.length&&!friendVisited) friendVisited=visited[v++].equals(friend); 

        if (maxDepth>0&&!friendVisited&&friend.doYouKnow(fred,maxDepth-1,visited)) {
          answer = true;
          if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
        }
      }
    }
    return answer;
  }

}
