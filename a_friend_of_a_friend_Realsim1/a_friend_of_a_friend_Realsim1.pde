Group group = new Group();
int AMOUNT_OF_PEOPLE = 2;
int MIN_AMOUNT_OF_FRIENDS = 2;
int MAX_AMOUNT_OF_FRIENDS = 5;
int MAXLINK = 6;
PFont font;

int fredId;

void setup() {
  
  size(640,480,P3D);
  ellipseMode(CENTER);
  font = loadFont("ArialMT-12.vlw");
  textFont(font,12);
  
  //curve
  //group.initNames();
  //group.reduceNamesTo(20);
  //group.initMyNames();
  
  //group.create(AMOUNT_OF_PEOPLE,MIN_AMOUNT_OF_FRIENDS,MAX_AMOUNT_OF_FRIENDS);  
  
  group.addPerson(new Person("Marcus",group.createEmptySpot()));
  group.personNamed("Marcus").addFriend(group,"Andrea");
  group.personNamed("Marcus").addFriend(group,"Wing");
  group.personNamed("Marcus").addFriend(group,"Tamara");
  
  group.addPerson(new Person("Wart",group.createEmptySpot()));
  group.personNamed("Wart").addFriend(group,"Peter");
  group.personNamed("Wart").addFriend(group,"Daphna");
  group.personNamed("Wart").addFriend(group,"Nico");
  group.personNamed("Wart").addFriend(group,"Tamara");
  
  group.addPerson(new Person("Nico",group.createEmptySpot()));
  group.personNamed("Nico").addFriend(group,"Olaf");
  
  group.addPerson(new Person("Olaf",group.createEmptySpot()));
  group.personNamed("Olaf").addFriend(group,"Joana");
  
  group.addPerson(new Person("Joana",group.createEmptySpot()));
  group.personNamed("Joana").addFriend(group,"Marcus");
  group.personNamed("Joana").addFriend(group,"Olaf");
  group.personNamed("Joana").addFriend(group,"Andrea");
  
  group.addPerson(new Person("Fred",group.createEmptySpot()));
  group.personNamed("Fred").addFriend(group,"Marcus");
  group.personNamed("Fred").addFriend(group,"Stev");
  
  group.addPerson(new Person("Stev",group.createEmptySpot()));
  group.personNamed("Stev").addFriend(group,"Marcus");
  group.personNamed("Stev").addFriend(group,"Andrea");
  
  //fredId = (int)random(group.size());
  fredId = group.indexOfPersonNamed("Olaf");
}


//void mousePressed() {
//  group.toggleShowStatus();
//}

void keyPressed() {
  if (key=='a') {
    group.setShowStatus(Person.LINKS_SHOW_ALL);
  } 
  else if (key=='s') {
    group.setShowStatus(Person.LINKS_SHOW_FIRST);
  }
  else if (key==' ') {
    fredId = (int)random(group.size());
  }
}

void draw() {
  colorMode(RGB,255);
  background(32,32,32);
  noStroke();
  group.hover();
  group.draw();
  Person fred = (Person)group.elementAt(fredId);
  group.drawNameAndFriends(fredId);
  group.makeIndexOfMouseover();
  
  colorMode(HSB,255);
  
  for (int i=0;i<group.size();i++) {
    //group.drawNameAndFriends(i);
    group.drawName(i);
  }
  
  if (group.INDEX_OF_MOUSEOVER!=-1&&group.INDEX_OF_MOUSEOVER!=fredId) {
    int i = group.INDEX_OF_MOUSEOVER;
    //group.drawNameAndFriends(i);
    Person mousePerson = (Person)group.elementAt(i);
    mousePerson.startSearchFor(fred,MAXLINK-1);
  }
  
  
  
}


color morphColor(color c) {
  float h = hue(c);
  h+=10;
  if (h>255) h-=255;
  return color(h,saturation(c),brightness(c));
}
