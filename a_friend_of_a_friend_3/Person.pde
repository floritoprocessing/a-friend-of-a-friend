class Person {

  float X=0, Y=0;
  String NAME="";
  int UNIQUE_ID=-1;
  //Vector FRIENDS = new Vector();
  String[] FRIENDS = new String[0];

  public static final float DRAW_SIZE = 2;
  float mouseSizeSq = 2*2;

  public static final int LINKS_SHOW_ALL = 0;
  public static final int LINKS_SHOW_FIRST = 1;
  public int LINKS_SHOW = LINKS_SHOW_FIRST;

  Person() {
  }

  Person(String name) {
    NAME = name;
  }

  Person(String name, float x, float y, int id) {
    this(name);
    X = x;
    Y = y;
    UNIQUE_ID = id;
  }
  
  Person(String name, float x, float y, int id, Vector friends) {
    this(name,x,y,id);
    //FRIENDS.addAll(friends);
    for (int i=0;i<friends.size();i++) FRIENDS = append(FRIENDS,((Person)friends.elementAt(i)).NAME);
  }
  
  boolean friendsContain(Person p) {
    for (int f=0;f<FRIENDS.length;f++) if (FRIENDS[f].equals(p.NAME)) return true;
    return false;
  }
  
  void addFriend(Person p) {
    FRIENDS = append(FRIENDS,p.NAME);
  }
  
  void addFriend(String name) {
    FRIENDS = append(FRIENDS,name);
  }

  void draw() {
    fill(128,128,128);
    ellipse(X,Y,DRAW_SIZE,DRAW_SIZE);
  }

  boolean mouseWithinRange() {
    return (sq(mouseX-X)+sq(mouseY-Y)<mouseSizeSq);
  }

  void showName(color c) {
    colorMode(RGB,255);
    fill(0,0,0,128);
    text(NAME,(int)X+2,(int)Y+2);
    fill(c);
    text(NAME,(int)X,(int)Y);
  }

  void drawNameAndFriends(Group g) {
    colorMode(RGB,255);
    stroke(255,255,255,64);
    for (int i=0;i<FRIENDS.length;i++) {
      Person p = g.getPersonNamed(FRIENDS[i]);
      //line(X,Y,((Person)FRIENDS.elementAt(i)).X,((Person)FRIENDS.elementAt(i)).Y);
      if (p!=null) line(X,Y,p.X,p.Y);
    }
    noStroke();
    showName(color(128,64,0));
  }

  boolean startSearchFor(Group g, Person fred, int maxDepth) {
    Person[] visited = new Person[1];
    visited[0]=this;
    boolean directFriendFound = false;
    for (int f=0;f<FRIENDS.length;f++) {
      Person friend = g.getPersonNamed(FRIENDS[f]);//(Person)FRIENDS.elementAt(f);
      if (friend!=null)
        if (friend.areYou(fred,visited)) {
          directFriendFound=true;
          if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
        }
    }
    if (!directFriendFound) {
      for (int f=0;f<FRIENDS.length;f++) {
        Person friend = g.getPersonNamed(FRIENDS[f]);//(Person)FRIENDS.elementAt(f);
        if (friend!=null && friend.doYouKnow(g,fred,maxDepth-1,visited)) {
          if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
        }
      }
    }
    return false;
  }
  
  void personCurve(Person[] p, int a0, int p0, int p1, int a1) {
    if (a0<0) a0=0;
    if (a1>p.length-1) a1=p.length-1;
    curve(p[a0].X,p[a0].Y,p[p0].X,p[p0].Y,p[p1].X,p[p1].Y,p[a1].X,p[a1].Y);
  }

  boolean areYou(Person fred, Person[] _visited) {
    if (this.equals(fred)) {
      
      Person[] p = new Person[_visited.length+1];
      for (int i=0;i<_visited.length;i++) p[i]=_visited[i];
      p[_visited.length]=this;
      
      colorMode(RGB,255);
      stroke(random(128,255),random(128,255),random(128,255));
      noFill();
      for (int i=0;i<p.length-1;i++) personCurve(p,i-1,i,i+1,i+2);
      for (int i=1;i<p.length-1;i++) p[i].showName(color(255,192,66));

    }
    return (this.equals(fred));
  }
  

  boolean doYouKnow(Group g, Person fred, int maxDepth, Person[] _visited) {
    // create a new visited list (visited People + this)
    Person[] visited = new Person[_visited.length+1];
    for (int i=0;i<_visited.length;i++) visited[i]=_visited[i];
    visited[visited.length-1]=this;
    
    boolean answer = false;
    boolean directFriendFound = false;
    
    // is one of my friends the target?
    for (int f=0;f<FRIENDS.length;f++) {
      Person friend = g.getPersonNamed(FRIENDS[f]);//(Person)FRIENDS.elementAt(f);
      
      boolean friendVisited=false;
      int v=0;
      while (v<visited.length&&!friendVisited) friendVisited=visited[v++].equals(friend); 

      if (maxDepth>=0&&!friendVisited&&friend!=null&&friend.areYou(fred,visited)) {
        answer = true;
        directFriendFound = true;
        if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
      } 
    }
    
    // if not direct friend, is a friend of a friend the target?
    if (!directFriendFound) {
      for (int f=0;f<FRIENDS.length;f++) {
        Person friend = g.getPersonNamed(FRIENDS[f]);//(Person)FRIENDS.elementAt(f);
        
        boolean friendVisited=false;
        int v=0;
        while (v<visited.length&&!friendVisited) friendVisited=visited[v++].equals(friend); 
        
        if (maxDepth>0&&!friendVisited&&friend!=null&&friend.doYouKnow(g,fred,maxDepth-1,visited)) {
          answer = true;
          if (LINKS_SHOW==LINKS_SHOW_FIRST) return true;
        }
      }
    }
    return answer;
  }

}
