Group group = new Group();
int AMOUNT_OF_PEOPLE = 5000;
int AMOUNT_OF_FRIENDS = 5;
PFont font;

void setup() {
  
  size(640,480,JAVA2D);
  ellipseMode(CENTER);
  font = loadFont("ArialMT-12.vlw");
  textFont(font,12);
  
  group.initNames();
  group.create(AMOUNT_OF_PEOPLE);  
  group.makeFriends(AMOUNT_OF_FRIENDS);
  
}

void mousePressed() {
  group.clear();
  group.create(AMOUNT_OF_PEOPLE);  
  group.makeFriends(AMOUNT_OF_FRIENDS);
}


void draw() {
  background(224,244,255);
  //noStroke();
  group.draw();
  group.drawNameAndFriends(0);
  group.makeIndexOfMouseover();
  
  if (group.INDEX_OF_MOUSEOVER>0) {
    group.drawNameAndFriends(group.INDEX_OF_MOUSEOVER);
    int MAXLINK=6;
    int depth=0;
    boolean found = false;
    while (!found) {
      found = ((Person)group.elementAt(group.INDEX_OF_MOUSEOVER)).findFriend((Person)group.elementAt(0),new Vector(),depth);
      depth++;
      if (depth>MAXLINK) found=true;
    }
    
  }
  
  
  
}
