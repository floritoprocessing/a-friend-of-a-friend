class Person {

  float X=0, Y=0;
  String NAME="";
  Vector FRIENDS = new Vector();

  float drawSize = 2;
  float mouseSizeSq = 2*2;

  Person() {
  }

  Person(String name) {
    NAME = name;
  }

  Person(String name, float x, float y) {
    this(name);
    X = x;
    Y = y;
  }

  void draw() {
    noStroke();
    fill(255,192,32);
    ellipseMode(RADIUS);
    ellipse(X,Y,drawSize,drawSize);
  }

  boolean mouseWithinRange() {
    return (sq(mouseX-X)+sq(mouseY-Y)<mouseSizeSq);
  }
  
  void showName() {
    noStroke();
    fill(0,0,0,64);
    text(NAME,(int)X+2,(int)Y+2);
    fill(128,64,0);
    text(NAME,(int)X,(int)Y);
  }

  void drawNameAndFriends() {
    stroke(0,0,0,64);
    for (int i=0;i<FRIENDS.size();i++) line(X,Y,((Person)FRIENDS.elementAt(i)).X,((Person)FRIENDS.elementAt(i)).Y);
    noStroke();
    showName();
  }

  boolean findFriend(Person to, Vector links, int depth) {
//    links.add(this);
    boolean friendFound = false;
    if (depth==0) {
      for (int i=0;i<FRIENDS.size();i++) {
        Person friend = (Person)FRIENDS.elementAt(i);
//        if (!links.contains(friend)) {
          if (friend.equals(to)) {
            friendFound = true;
            stroke(255,0,0);
            line(X,Y,friend.X,friend.Y);
          }
//        }
      }
    }
    else {
      boolean nextFriendFound = false;
      for (int i=0;i<FRIENDS.size();i++) {
        nextFriendFound = (((Person)FRIENDS.elementAt(i)).findFriend(to,links,depth-1));
        if (nextFriendFound) {
          stroke(255,0,0);
          line(X,Y,((Person)FRIENDS.elementAt(i)).X,((Person)FRIENDS.elementAt(i)).Y);
          ((Person)FRIENDS.elementAt(i)).showName();
          i=FRIENDS.size(); // JUST SHOW FIRST FOUND CONNECTION
        }
      }
      if (nextFriendFound) friendFound = true;
    }
    return friendFound;
  }

}
