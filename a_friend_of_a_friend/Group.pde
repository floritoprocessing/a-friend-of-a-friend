import java.util.Vector;

class Group extends Vector {
  
  float FAR_FRIEND_CHANCE = 0.01;
  float FRIEND_DISTANCE_DIVIDER = 10;
  String[] boyName, girlName;
  
  int INDEX_OF_MOUSEOVER=-1;
  
  Group() {
    super();
  }
  
  void initNames() {
    // load names
    // http://www.listofbabynames.org/k_girls.htm
    boyName = loadStrings("boys.TXT");
    girlName = loadStrings("girls.TXT");
    println(boyName.length+" boy names and "+girlName.length+" girl names");
  }

  void create(int amount) {
    // CREATE GROUP
    for (int i=0;i<amount;i++) {
      String name = (random(1.0)<0.5)?boyName[(int)random(boyName.length)]:girlName[(int)random(girlName.length)];
      add(new Person(name,random(width),random(height)));
    }
  }



  void makeFriends(int amount) {
    // MAKE FRIENDS:
    float maxDist = sqrt(sq(width)+sq(height));
    for (int i=0;i<size();i++) {
      Person person = (Person)elementAt(i);
      Person newFriend = new Person();
      for (int f=person.FRIENDS.size();f<amount;f++) {
        int friendIndex=-1;
        boolean foundFriend=false;
        
        if (random(1.0)<FAR_FRIEND_CHANCE) {
          newFriend = (Person)elementAt((int)random(size()));
          if (!newFriend.equals(person)) foundFriend=true;
        }
        
        while (!foundFriend) {
          friendIndex = (int)random(size());
          newFriend = (Person)elementAt(friendIndex);
          if ((!newFriend.equals(person))&&(!person.FRIENDS.contains(newFriend))) {
            float dis = sqrt(sq(person.X-newFriend.X)+sq(person.Y-newFriend.Y));
            float friendChance = 1.0 - FRIEND_DISTANCE_DIVIDER*dis/maxDist;
            if (random(1.0)<friendChance) foundFriend=true;
          }
        }
        person.FRIENDS.add(newFriend);
        newFriend.FRIENDS.add(person);
      }
    }
  }
  
  
  void draw() {
    for (int i=0;i<size();i++) ((Person)elementAt(i)).draw();
  }
  
  void drawNameAndFriends(int i) {
    Person p = ((Person)elementAt(i));
    p.drawNameAndFriends();
  }
  
  void makeIndexOfMouseover() {
    INDEX_OF_MOUSEOVER=-1;
    for (int i=0;i<size();i++) {
      Person p = ((Person)elementAt(i));
      if (p.mouseWithinRange()) INDEX_OF_MOUSEOVER = i;
    }
  }

}
