class Person {
  
  float X=0, Y=0;
  float drawSize = 2;
  float mouseSizeSq = 2*2;
  String NAME="";
  
  Person(String name) {
    NAME = name;
  }
  
  Person(String name, float x, float y) {
    this(name);
    X = x;
    Y = y;
  }
  
  void draw() {
    ellipse(X,Y,drawSize,drawSize);
    if (sq(mouseX-X)+sq(mouseY-Y)<mouseSizeSq) text(NAME,(int)X,(int)Y);
  }
  
}
