Vector group = new Vector();
String[] boyName, girlName; //http://www.listofbabynames.org/k_girls.htm
float distance = 15;
PFont font;

void setup() {
  size(640,480,P3D);
  ellipseMode(CENTER_RADIUS);
  boyName = loadStrings("boys.TXT");
  girlName = loadStrings("girls.TXT");
  println(boyName.length+" boy names and "+girlName.length+" girl names");
  
  String name = (random(1.0)<0.5)?boyName[(int)random(boyName.length)]:girlName[(int)random(girlName.length)];
  for (int i=0;i<1500;i++) group.add(new Person(name,random(width),random(height)));
  
  font = loadFont("ArialMT-12.vlw");
  textFont(font,12);
}

void draw() {
  background(224,244,255);
  fill(0);
  for (int i=0;i<group.size();i++) ((Person)group.elementAt(i)).draw();
}
