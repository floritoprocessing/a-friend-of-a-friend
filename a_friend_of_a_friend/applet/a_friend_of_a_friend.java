import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class a_friend_of_a_friend extends PApplet {Vector group = new Vector();
String[] boyName, girlName; //http://www.listofbabynames.org/k_girls.htm
float distance = 15;
PFont font;

public void setup() {
  size(640,480,P3D);
  ellipseMode(CENTER_RADIUS);
  boyName = loadStrings("boys.TXT");
  girlName = loadStrings("girls.TXT");
  println(boyName.length+" boy names and "+girlName.length+" girl names");
  
  String name = (random(1.0f)<0.5f)?boyName[(int)random(boyName.length)]:girlName[(int)random(girlName.length)];
  for (int i=0;i<1500;i++) group.add(new Person(name,random(width),random(height)));
  
  font = loadFont("ArialMT-12.vlw");
  textFont(font,12);
}

public void draw() {
  background(224,244,255);
  fill(0);
  for (int i=0;i<group.size();i++) ((Person)group.elementAt(i)).draw();
}
class Person {
  
  float X=0, Y=0;
  float drawSize = 2;
  float mouseSizeSq = 2*2;
  String NAME="";
  
  Person(String name) {
    NAME = name;
  }
  
  Person(String name, float x, float y) {
    this(name);
    X = x;
    Y = y;
  }
  
  public void draw() {
    ellipse(X,Y,drawSize,drawSize);
    if (sq(mouseX-X)+sq(mouseY-Y)<mouseSizeSq) text(NAME,(int)X,(int)Y);
  }
  
}
static public void main(String args[]) {   PApplet.main(new String[] { "a_friend_of_a_friend" });}}